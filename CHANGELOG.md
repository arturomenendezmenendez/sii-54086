# Cangelog
Los cambios del proyecto seran documentados en este archivo.

El formato esta basado en [Keep a Changelog] (https://keepachangelog.com)


## [1.3.0] - 2021-11-22
## Added
- Ficheros logger.txt y Logger.cpp con las puntuaciones
- Tubería FIFO en Mundo.h y Mundo.cpp
- bot.cpp que genera un programa que da inteligencia a la raqueta1
- Si uno de los jugadores anota 5 puntos el juego acabara

## [1.2.2] - 2021-10-26
## Added
- Añadido movimiento de las raquetas
- La esfera disminuye de tamaño con el tiempo

## [1.2.1] - 2021-10-26
## Added
- Añadido movimiento de la esfera

## [1.2.0] - 2021-10-26
## Added
- Creación del README.md

## [1.1.0] - 2021-10-24
## Added
- Creación de este CHANGELOG.md
